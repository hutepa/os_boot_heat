## Bootstrap Openstack pristine image with heat config agents

This template can be used to bootstrap a pristine Redhat image with agents
 required to use heat software deployment resources in
templates. 

This only installs the heat-config-script and heat-config-ansible hooks. 

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
